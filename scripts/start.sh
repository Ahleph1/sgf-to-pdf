pkill nodejs && pkill node && pkill redis-server

# handle nodejs being "node" on mac and "nodejs" on linux
if [[ "$(uname)" == "Linux" ]]; then
    platform='linux'
else
    platform='not linux'
fi
if [[ "$platform" == "linux" ]]; then
    nodejs app.js
elif [[ "$platform" == "not linux" ]]; then
    node app.js
fi
