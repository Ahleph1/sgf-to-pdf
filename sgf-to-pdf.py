import sys, os
import re


with open(sys.argv[1], 'r+') as sgf:
  texString = "\\documentclass[a4paper,12pt]{article}\n\\usepackage{psgo}\n\\begin{document}\n\\begin{psgoboard}\n"
  sgf_data = sgf.read()
  black_moves = re.findall("B(\[[a-t][a-t]\])+", sgf_data)
  white_moves = re.findall("W(\[[a-t][a-t]\])+", sgf_data)
  print white_moves
  print black_moves
  for move in black_moves:
    # we skip i, incrementing the letter
    if ord(move[1]) >= 105:
      texString += "\\stone{black}{"+chr(ord(move[1])+1)+"}{"+str(ord(move[2])-96)+"}\n"
    else:
      texString += "\\stone{black}{"+move[1]+"}{"+str(ord(move[2])-96)+"}\n"
  for move in white_moves:
    if ord(move[1]) >= 105:
      texString += "\\stone{white}{"+chr(ord(move[1])+1)+"}{"+str(ord(move[2])-96)+"}\n"
    else:
      texString += "\\stone{white}{"+move[1]+"}{"+str(ord(move[2])-96)+"}\n"

  texString += "\\end{psgoboard}\n"
  texString += "\\end{document}"

  print texString

  os.system("touch {0}.tex".format(sys.argv[1][:-4]))
  tex_file = open("{0}.tex".format(sys.argv[1][:-4]), "r+")
  tex_file.write(texString)
  tex_file.close()

os.system("latex {0}.tex && dvips -P pdf {0}.dvi && ps2pdf {0}.ps".format(sys.argv[1][:-4]))
