# Overview

Source code for http://sgf-pdf.xyz.

# What does it do?

This utility takes a single, unbranching SGF file (e.g. a game record) for the game of Go/Weiqi/Baduk,
sends it to the server for processing,
converts it to PDF using LaTeX, then sends the result to the user as a download.

# Host Locally

```git clone``` this repo, then run ```npm install``` followed by ```node app.js``` to run on http://localhost:3000.

You will also need ```latex```, ```dvips```, ```ps2pdf```, and ```python``` installed.
###### Note: Not tested with Python 3. use Python 2 (2.6+) for best results.
