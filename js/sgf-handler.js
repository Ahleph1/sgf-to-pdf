$(function(){
    var socket = io.connect('http://localhost:3000');
    $("#nav").css("width", window.innerWidth)
    socket.on('connect', function(){
        var delivery = new Delivery(socket);

        delivery.on('delivery.connect',function(delivery){
            $("input[type=submit]").click(function(evt){
                var file = $("input[type=file]")[0].files[0];
                if (file.size < 5500000) {
                    var extraParams = {uuid: create_id()};
                    delivery.send(file, extraParams);
                    $("#loading").css("visibility", "visible")
                    evt.preventDefault();
                } else {
                    alert("Max file size: 5MB")
                }
            });
        });

        socket.on('conversion_complete', function(pdf_id) {
            $("#loading").css("visibility", "hidden");
            window.location.href = "http://localhost:3000/download/" + pdf_id;
        })
        socket.on('conversion_error', function(err) {
            $("#loading").css("visibility", "hidden");
            alert(err);
        })
    });
});



function create_id() {
    return (((3333333333 - (Math.random())*1000000000))).toString(16).substring(0,6);
}
