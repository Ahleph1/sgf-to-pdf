var http = require('http');
var express = require('express');
var io = require('socket.io')(http);
var path = require('path');
var assert = require('assert');
var dl = require('delivery');
var fs = require('fs');
var PythonShell = require('python-shell');
// Express routing
var app = express();
// Set root directory for application
app.use(express.static('.'));

// Render index.html in browser
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

app.get('/download/[0-9a-f]+[0-9a-f]+', function(req, res) {
    // get ID of pdf: the 8 digits of url
    id = req.url.substr(req.url.length - 6)
    res.download('pdf-' + id + '.pdf')
})

io.sockets.on('connection', function(socket){
    var delivery = dl.listen(socket);
    delivery.on('receive.success',function(file){
        var params = file.params;
        fileUuid = params["uuid"]
        fs.writeFile("pdf-" + fileUuid+'.sgf', file.buffer, function(err){
            if(err){
              console.log('File could not be saved.');
            }else{
                console.log('File saved.');
                PythonShell.run('sgf-to-pdf.py', {args:['pdf-'+fileUuid+'.sgf', 0]}, function(err) {
                    if (err) {
                        null
                    }
                    socket.emit('conversion_complete', fileUuid);
                })
            }
        });
    });
});

io.listen(app.listen(3000));
